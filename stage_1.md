# Stage 1
> Welcome in the airhockey hackathon repo. Here you'll find the main project along with the files necessary for the different stages.

## The main project structure is as follows:
    .
    └── Hackathon
    │    └── Assets
    │    |    ├── Brains                      # Player and Learning brains
    │    |    ├── Materials                   # Materials used by the prefabs
    │    |    ├── ML-Agents                   # ML-Agents plugin source files
    │    |    ├── Models                      # Trained neural network models for the brains to use
    │    |    ├── Physics_Material            # Physics materials used by the prefabs
    │    |    ├── Prefabs                     # Prefabs for field, paddle, puck and stage
    │    |    ├── Scenes                      # The different stages
    │    |    └── Scripts                     # C# magic that makes it all work
    │    ├── Temp
    │    ├── Library
    │    ├── ProjectSettings
    │    ├── Packages
    │    ├── obj
    │    └── Logs
    └── ML
        ├── summaries                         # Tensorboard statistics
        ├── models                            # Saved models
        └── config                            # Training config

Throughout the hackathon you'll be working changing up scripts in the Scripts folder, creating suitable brains for your code to run with in the Brains folder and put your trained neural network models in the Models folder.

## Documentation

https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Readme.md

By far the most useful documentation regarding ML-agents and the training process can be found here. They have excellent tutorials and guides. If you find yourself stuck or not knowing what to do chances are high you'll find the answers somewhere in this documentation.

# Assets: Unity

## Prefabs

We created three basic prefabs: the field, the player and the puck. These are the basic building blocks of the game.

We combined them into a 'Stage 1' prefab. This is the unit you'll want to be working most with. It has all the correct connectors between the other prefabs and scripts already set.

## Brains

We created one Playerbrain and a Learningbrain. You can use the Playerbrain to move the agent yourself with wasd. The Learningbrain has to be connected to the external mlagents-learn command.

## Scenes

There will be one scene: stage 1. This is where you can setup you training environment. An academy object is already created and linked up.

## Scripts

This is the main folder for the hackathon. You'll find three files in this folder.

  * ControllerStage1
  * PuckStage1
  * AgentStage1

*ControllerStage1* is applied to the field and manages the general game elements like initialising the game, position resetting, keeping track of scoring etc.

*PuckStage1* does nothing else than detect when it is touched by the agent and lets the controller know about it.

*AgentStage1* is where you will work in. The script is applied to the player paddle object. Here is where you make the agent.

# ML: mlagents-learn command

## summaries
When you train models using the mlagents-learn command, you're going to have to add a summaries folder for tensorflow to save statistics inside of. This folder will then be used by tensorboard to visualize how training is going

## models
Saved model checkpoints are save here in both tensorflow format as .nn unity format. You can copy any .nn file from here to Hackathon/Assets/Models to run the trained model and inspect its performance.

## config
Any config files used by the mlagents-learn command can be put in here.


# Objective:

Stage 1 objective is training an agent to touch the puck.

# Evaluation:

You can train the agent in any way you want, but in the end you'll send us only the following:

  * The Learningbrain
  * The trained model
  * Your agentscript

This means that you have to know what you change outside of the these files, because that change won't be there when we test it on our own machines. Changing the respawn position can be a good strategy for training, but changing the physics settings might train your agent wrong and so not work on our testing machines.

The ranking will be based by how fast the agent can touch 100 pucks. To make sure that this time is based on the agent itself and not the speed of the agent, the evaluation will happen with an agent that has a speed of 3. We also invite you to share how fast your agent trained. In other words, how many steps did it take to train the agent.

# Localposition

We used Unity's localPosition function for almost all coordinate related things in the code. Please do this too. In the end, we want to be able to create duels and we don't want the agent to be confused due to being on the side of the field it never trained on.

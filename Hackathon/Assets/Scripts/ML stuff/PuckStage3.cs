﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PuckStage3 : MonoBehaviour
{
    public ControllerStage3 controllerScript;

    /// <summary>
    /// This function gets called when the object first touches another object.
    /// </summary>
    void OnCollisionEnter(Collision col)
    {
        // Scored Goal
        if (col.gameObject.CompareTag("goal_blue"))
        {
            controllerScript.Scored("goal_blue");
        }
        // Lost Goal
        if (col.gameObject.CompareTag("goal_red"))
        {
            controllerScript.Scored("goal_red");
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class AgentStage3 : Agent
{

    
    public AirhockeyAcademy academy;
    public ControllerStage3 controllerScript;
    public string side = "blue";
    public float speed = 3f;
    private Rigidbody rBody;
    private Dictionary<string, int> side_value;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
        side_value = new Dictionary<string, int>()
                                            {
                                                {"red", -1},
                                                {"blue", 1},
                                            };
    }

    /// <summary>
    /// Reset the agent. This function is called after the agent complete an episode.
    /// </summary>
    public override void AgentReset()
    {
        controllerScript.ResetPosition();
    }

    /// <summary>
    /// Define and collect the observations. 
    /// MAKE SURE TO TAKE INTO ACCOUNT THE SIDE THE AGENT IS PLAYING.
    /// </summary>
    public override void CollectObservations()
    {

    }

    /// <summary>
    /// Moves the agent according to the selected action.
    /// </summary>
	public void MoveAgent(float[] act)
    {
        Vector3 dirToGo = Vector3.zero;
        pushAgent(dirToGo);
    }

    /// <summary>
    /// Execute an action.
    /// </summary>
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        MoveAgent(vectorAction);
    }

    /// <summary>
    /// Push the agent in the given direction. The direction is mutiplied by the side of the agent to allow the agent to play both sides without having to train the agent on both sides.
    /// </summary>
    private void pushAgent(Vector3 dirToGo)
    {
        rBody.AddForce(dirToGo * speed * side_value[side], ForceMode.VelocityChange);
    }
}

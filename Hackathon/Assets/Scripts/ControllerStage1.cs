﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControllerStage1 : MonoBehaviour
{
    public Rigidbody player;
    public Transform puck;
    public Transform field;
    public bool evaluate = false;
    public Text scoreText;
    public Text timerText;

    private int touchedPucks;
    private int goalPucksTouched;
    private float time;
    private Vector3 puckStartP;
    private Vector3 playerStartP;
    void Start()
    {
        touchedPucks = 0;
        goalPucksTouched = 100;
        time = 0;
        scoreText.text = getScoreString();
        scoreText.gameObject.SetActive(this.evaluate);
        timerText.gameObject.SetActive(this.evaluate);
        playerStartP = player.position;
        puckStartP = puck.position;
    }
    void Update()
    {
        if (this.evaluate & touchedPucks < goalPucksTouched)
        {
            time += Time.deltaTime;
            timerText.text = "Time: " + time.ToString("F3") + "s";
        }
        if (this.evaluate & touchedPucks == goalPucksTouched)
        {
            puck.transform.position = puckStartP;
            player.transform.position = playerStartP;
        }
        if (player.transform.localPosition.y < field.localPosition.y || puck.localPosition.y < field.localPosition.y)
        {
            ResetPosition();
        }
    }

    public void ResetPosition()
    {
        if (this.evaluate == false | touchedPucks < goalPucksTouched)
        {
            player.angularVelocity = Vector3.zero;
            player.velocity = Vector3.zero;
            puck.GetComponent<Rigidbody>().velocity = Vector3.zero;

            float randomX = Random.Range(-0.65f, 0.65f);
            float randomZ = Random.Range(-1.1f, 1.1f);

            // Move Player to Starting point
            player.transform.localPosition = new Vector3(randomX, field.localPosition.y + 0.015f, randomZ);

            randomX = Random.Range(-0.65f, 0.65f);
            randomZ = Random.Range(-1.1f, 1.1f);

            // Move the Puck to a new spot
            puck.localPosition = new Vector3(randomX, field.localPosition.y + 0.015f, randomZ);

        }
    }

    // Update is called once per frame
    public void PuckTouched()
    {
        if (this.evaluate)
        {
            touchedPucks++;
            scoreText.text = getScoreString();
        }
    }



    private string getScoreString()
    {
        string scoreText = "";
        if (touchedPucks < 10)
        {
            scoreText = "Pucks touched: 00" + touchedPucks + "/" + goalPucksTouched.ToString();
        }
        else
        {
            if (touchedPucks < 100)
            {
                scoreText = "Pucks touched: 0" + touchedPucks + "/" + goalPucksTouched.ToString();
            }
            else
            {
                scoreText = "Pucks touched: " + touchedPucks + "/" + goalPucksTouched.ToString();
            }
        }
        return scoreText;
    }
}

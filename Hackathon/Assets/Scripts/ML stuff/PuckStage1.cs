using UnityEngine;

public class PuckStage1 : MonoBehaviour
{
    public ControllerStage1 controllerScript;

    /// <summary>
    /// This function gets called when the object first touches another object.
    /// </summary>
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            controllerScript.PuckTouched();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class AgentStage2 : Agent
{
    public AirhockeyAcademy academy;
    public ControllerStage2 ControllerScript;
    Rigidbody rBody;
    public float speed = 3f;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Reset the agent. This function is called after the agent complete an episode.
    /// </summary>
    public override void AgentReset()
    {
        ControllerScript.ResetPosition();

    }

    /// <summary>
    /// Define and collect the observations.
    /// </summary>
    public override void CollectObservations()
    {

    }

    /// <summary>
    /// Moves the agent according to the selected action.
    /// </summary>
	public void MoveAgent(float[] act)
    {
        Vector3 dirToGo = Vector3.zero;
        pushAgent(dirToGo);
    }

    /// <summary>
    /// Execute an action.
    /// </summary>
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        MoveAgent(vectorAction);
    }

    /// <summary>
    /// Push the agent in the given direction.
    /// </summary>
    private void pushAgent(Vector3 dirToGo)
    {
        rBody.AddForce(dirToGo * speed, ForceMode.VelocityChange);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerStage2 : MonoBehaviour
{
    public Rigidbody player;
    public Transform puck;
    public Transform field;
    public bool evaluate = false;
    public Text scoreText;
    public Text timerText;
    private int score;
    private float time;
    private Vector3 puckStartP;
    private Vector3 playerStartP;

    void Start()
    {
        time = 60;
        scoreText.text = "Score :" + score.ToString();
        scoreText.gameObject.SetActive(this.evaluate);
        timerText.gameObject.SetActive(this.evaluate);
        playerStartP = player.position;
        puckStartP = puck.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.evaluate & time > 0)
        {
            time -= Time.deltaTime;
            timerText.text = "Time: " + time.ToString("F3") + "s";
        }
        if (this.evaluate & time <= 0)
        {
            puck.transform.position = puckStartP;
            player.transform.position = playerStartP;
            timerText.text = "Time: 0.000";
        }
        if (player.transform.localPosition.y < field.localPosition.y || puck.localPosition.y < field.localPosition.y)
        {
            ResetPosition();
        }
    }

    public void ResetPosition()
    {
        
        if (this.evaluate == false | time > 0)
        {
            float randomX = Random.Range(-0.65f, 0.65f);
            float randomZ = Random.Range(-1.1f, 1.1f);
            puck.localPosition = new Vector3(randomX, field.localPosition.y + 0.02f, randomZ);
            puck.GetComponent<Rigidbody>().velocity = Vector3.zero;

            if (this.evaluate)
            {
                if (randomZ < 0)
                {
                    float speedX = Random.Range(-0.9f, 0.9f);
                    puck.GetComponent<Rigidbody>().velocity = new Vector3(speedX * 2, 0, 2);
                }
            }
            player.angularVelocity = Vector3.zero;
            player.velocity = Vector3.zero;
            randomX = Random.Range(-0.65f, 0.65f);
            randomZ = Random.Range(0.1f, 1.1f);
            player.transform.localPosition = new Vector3(randomX, field.localPosition.y + 0.02f, randomZ);
        }
        
    }

    // Update is called once per frame
    public void Scored(int goal)
    {
        if (this.evaluate)
        {
            score += 1 * goal;
            scoreText.text = "Score : " + score.ToString();
        }
    }

}

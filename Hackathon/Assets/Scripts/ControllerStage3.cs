﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerStage3 : MonoBehaviour
{
    public Rigidbody red;
    public Rigidbody blue;
    public Transform puck;
    public Transform field;
    public bool evaluate = false;
    public Text scoreRedText;
    public Text scoreBlueText;
    public Text victoryText;

    private int maxScore;
    private int scoreRed;
    private int scoreBlue;
    private Vector3 puckStartP;
    private Vector3 redStartP;
    private Vector3 blueStartP;

    void Start()
    {
        maxScore = 11;
        scoreRedText.gameObject.SetActive(this.evaluate);
        scoreBlueText.gameObject.SetActive(this.evaluate);
        blueStartP = blue.position;
        redStartP = red.position;
        puckStartP = puck.position;
        victoryText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.evaluate & (scoreRed >= maxScore | scoreBlue >= maxScore))
        {
            puck.transform.position = puckStartP;
            red.transform.position = redStartP;
            blue.transform.position = blueStartP;
        }
        if (this.evaluate & scoreRed >= maxScore)
        {
            victoryText.color = new Color(255, 0, 0);
            victoryText.gameObject.SetActive(true);
        }
        if (this.evaluate & scoreBlue >= maxScore)
        {
            victoryText.color = new Color(0, 0, 255);
            victoryText.gameObject.SetActive(true);
        }
        if (red.transform.localPosition.y < field.localPosition.y || blue.transform.localPosition.y < field.localPosition.y || puck.localPosition.y < field.localPosition.y)
        {
            ResetPosition();
        }
    }

    public void ResetPosition()
    {
        puck.position = puckStartP;
        puck.GetComponent<Rigidbody>().velocity = Vector3.zero;

        if (this.evaluate == false | (scoreRed < maxScore & scoreBlue < maxScore))
        {
            red.angularVelocity = Vector3.zero;
            red.velocity = Vector3.zero;
            red.transform.position = redStartP;

            blue.angularVelocity = Vector3.zero;
            blue.velocity = Vector3.zero;
            blue.transform.position = blueStartP;

            float randomX = Random.Range(-0.65f, 0.65f);
            float randomZ = Random.Range(-1.1f, 1.1f);

            puck.localPosition = new Vector3(randomX, field.localPosition.y + 0.02f, randomZ);
        }
    }

    public void Scored(string goal)
    {
        if (this.evaluate)
        {
            if(goal == "goal_red")
            {
                scoreBlue++;
                if (scoreBlue < 10)
                {
                    scoreBlueText.text = "0" + scoreBlue.ToString();
                }
                else
                {
                    scoreBlueText.text = scoreBlue.ToString();
                }
            }
            if (goal == "goal_blue")
            {
                scoreRed++;
                if(scoreRed < 10){
                    scoreRedText.text = "0"  + scoreRed.ToString();
                }
                else
                {
                    scoreRedText.text = scoreRed.ToString();
                }
            }
            ResetPosition();
        }
    }

}

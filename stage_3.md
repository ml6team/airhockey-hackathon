# Stage 3
> Now it's time to make a scoring agent into an actually good player that's hard to win from as human.

# Assets: Unity

## Prefabs

A new prefab was made for this stage called 'Stage 3' (innovative, we know, again). This is probably the starting point you're going to want to use. Of course you're free to change the setting as you wish as long as you don't change values that will influence the evaluation.

## Brains

There is a new Learningbrain for this stage as well. Can be a copy of the previous stage brain if you wish, the different name makes the trainingfiles names more clear though.

## Scenes

There will be a last scene: stage 3. This is where you can setup you training environment. An academy object is already created and linked up.

## Scripts

This is the main folder for the hackathon. You'll find the three necessary files for this stage in this folder.

  * ControllerStage3
  * PuckStage3
  * AgentStage3

*ControllerStage3* is applied to the field and manages the general game elements like initialising the game, position resetting, keeping track of scoring etc.

*PuckStage3* does nothing else than detect when it is touched by the agent and lets the controller know about it.

*AgentStage3* is where you will work in. The script is applied to the player paddle object. Here is where you make the agent.


# Objective:

Stage 3 objective is training an agent to play the real game as good as possible.

# Evaluation:

You can train the agent in any way you want, but in the end you'll send us only the following:

  * The Learningbrain
  * The trained model
  * Your agentscript

This means that you have to know what you change outside of the these files, because that change won't be there when we test it on our own machines. Changing the respawn position can be a good strategy for training, but changing the physics settings might train your agent wrong and so not work on our testing machines.

The ranking will be based by playing games against other trained agents in a tournament style. To make sure that this time is based on the agent itself and not the speed of the agent, the evaluation will happen with an agent that has a speed of 3. We also invite you to share how fast your agent trained. In other words, how many steps did it take to train the agent. How many games played and the exact format of the tournament will depend on the amount of teams present.

# Localposition

We used Unity's localPosition function for almost all coordinate related things in the code. Please do this too. In the end, we want to be able to create duels and we don't want the agent to be confused due to being on the side of the field it never trained on.
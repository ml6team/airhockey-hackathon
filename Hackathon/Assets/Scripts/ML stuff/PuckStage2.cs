using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PuckStage2 : MonoBehaviour
{
    public ControllerStage2 controllerScript;

    /// <summary>
    /// This function gets called when the object first touches another object.
    /// </summary>
    void OnCollisionEnter(Collision col)
    {
        // Scored Goal
        if (col.gameObject.CompareTag("Goal"))
        {
            controllerScript.Scored(1);

        }
        // Lost Goal
        if (col.gameObject.CompareTag("OwnGoal"))
        {
            controllerScript.Scored(-1);
        }
    }
}